# Upgrade-Tests

Für einen vollständigen Test muss alles von Grund auf erstellt werden. Ist der
Namespace noch erstellt, ist alles ok.

Diese Deployments testen die grundlegenden Infrastuktur-Kompenenten, die z. B. nach
Kubernetes-Upgrades möglicherweise nicht mehr funktionieren könnten. Dabei wird u. a.
ein Webserver und ein persistentes Volume eingerichtet, und ein darin ein
verschlüsseltes Secret als Inhalt abgelegt. Für den Webserver werden zwei Ingresses
eingerichtet, einer via nginx und einer via haproxy.

Aufsetzen: `kustomize build . | kubectl apply -f -`

Ein erfolgreicher Zugriff auf (in diesem Fall)

 - https://let-03-upgrade.ethz.ch/ (nginx)
 - https://let-03-upgrade-haproxy.ethz.ch/ (haproxy)

testet alle Komponenten.

Wird der Test auf einen anderen Cluster übertragen, müssen die Hostnamen in den
beiden Ingress-Definitionen angepasst werden!

Achtung: die von ingress-haproxy genutzte IP wird nicht vom alias-manager
verwaltet. Dieser Alias muss von Hand eingetragen werden (siehe IP-Konfiguration
in ../argocd/ingress-haproxy.yml in diesem Repository).

Abräumen: `kustomize build . | kubectl apply -f -`
